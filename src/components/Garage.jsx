import React, { useState } from 'react';

const Garage = () => {
  // Initialize with two basic cars using their image URLs
  const [ownedCars, setOwnedCars] = useState([
    { name: 'Basic Car 1', imageUrl: 'https://png.pngtree.com/png-clipart/20220625/ourmid/pngtree-car-sports-car-transportation-png-image_5320963.png' },
    { name: 'Basic Car 2', imageUrl: 'https://static.vecteezy.com/system/resources/previews/001/193/929/non_2x/vintage-car-png.png' }
  ]);

  const buyCar = (car) => {
    // Logic to add the purchased car to ownedCars
    setOwnedCars([...ownedCars, car]);
  };

  return (
    <div>
      <h1>Garage</h1>
      <h2>Owned Cars:</h2>
      <div style={{ display: 'flex', justifyContent: 'space-between' }}>
        {ownedCars.map((car, index) => (
          <div key={index}>
            <h3>{car.name}</h3>
            <img src={car.imageUrl} alt={car.name} style={{ width: '200px', height: 'auto' }} />
          </div>
        ))}
      </div>
      <h2>Buy New Cars:</h2>
      <button onClick={() => buyCar({ name: 'New Car', imageUrl: 'URL_OF_NEW_CAR_IMAGE' })}>Buy New Car</button>
      {/* Display available cars to buy */}
    </div>
  );
};

export default Garage;
