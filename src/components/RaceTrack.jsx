import React, { useState, useEffect, useCallback } from 'react';
import { Parallax } from 'react-parallax';
import './raceTrack.css';
import Tachometer from './Tachometer/Tachometer';
import Car from './Car/Car';

const RaceTrack = () => {
  const [speed, setSpeed] = useState(5);
  const [treeSpeed, setTreeSpeed] = useState(0);
  const [treePosition, setTreePosition] = useState(window.innerWidth);
  const [isAccelerating, setIsAccelerating] = useState(false);

  const gearsData = {
    maxSpeed: 250,
    gears: 6,
    lengths: [
      { low: 1, max: 40 },
      { low: 41, max: 80 },
      { low: 81, max: 120 },
      { low: 121, max: 160 },
      { low: 161, max: 200 },
      { low: 201, max: 250 },
    ],
  };

  const calculateSpeedIncrease = (currentSpeed) => {
    const currentGear = gearsData.lengths.find(
      (gear) => currentSpeed >= gear.low && currentSpeed <= gear.max
    );

    if (currentGear) {
      const percentageInGear = (currentSpeed - currentGear.low) / (currentGear.max - currentGear.low);
      const speedIncrease = 50 * percentageInGear;
      return speedIncrease;
    }
    return 0;
  };

  useEffect(() => {
    const handleKeyPress = (event) => {
      if (event.code === 'Space') {
        const speedIncrease = calculateSpeedIncrease(speed);
        setSpeed((prevSpeed) => {
          const newSpeed = prevSpeed + speedIncrease;
          return Math.min(newSpeed, gearsData.maxSpeed);
        });
        setTreeSpeed((prevTreeSpeed) => prevTreeSpeed + speedIncrease / 10);
      }
    };

    document.addEventListener('keydown', handleKeyPress);

    return () => {
      document.removeEventListener('keydown', handleKeyPress);
    };
  }, [speed, gearsData.lengths]);

  const handleAccelerate = useCallback(() => {
    setIsAccelerating(true);
  }, []);

  useEffect(() => {
    const treeInterval = setInterval(() => {
      setTreePosition((prevPosition) => prevPosition <= 0 ? window.innerWidth : prevPosition - treeSpeed);
    }, 50);

    return () => {
      clearInterval(treeInterval);
    };
  }, [treeSpeed]);

  return (
    <Parallax
      bgImage="https://media.istockphoto.com/id/1081596948/vector/side-view-of-a-road-with-a-crash-barrier-roadside-green-meadow-and-clear-blue-sky-background.jpg?s=612x612&w=0&k=20&c=dikiJHfzZaDSy9Of4aMmEkqLxXMnHda2QqPguymZf-w="
      bgImageAlt="Main Background"
      bgImageStyle={{ width: "200vw", backgroundRepeat: "repeat-x" }}
      strength={200 + speed * 10 / 100}
      className="parallax-container"
      style={{ width: "100vw" }}
    >
      <div className="race-track-container">
        <Tachometer 
          speed={speed * 10} 
          rpm={speed * 10} 
          accelerate={handleAccelerate} 
          car={{
            currentSpeed: speed,
            gearsData: gearsData,
            increaseSpeed: () => setSpeed((prev) => prev + 4),
            performance: 200,
          }} 
        />
        <div className="cars-container">
          <Car speed={speed} />
          {`Speed: ${speed}, RPM: ${speed * 10}`}
        </div>
        {treePosition > 0 && (
          <img
            src="https://upload.wikimedia.org/wikipedia/en/c/c6/Simple_Tree_Clipart.png"
            alt="Rare Tree"
            className="rare-tree"
            style={{ left: `${treePosition}px` }}
          />
        )}
      </div>
    </Parallax>
  );
};

export default RaceTrack;
