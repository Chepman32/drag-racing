import React, { useState, useEffect, useCallback } from 'react';
import './tachometer.css';

const Tachometer = ({ speed, accelerate }) => {
  const [rotationAngle, setRotationAngle] = useState(0);

  useEffect(() => {
    if (speed > 0) {
      setRotationAngle(speed * 15); // Adjust the multiplier to get a realistic feel
    } else {
      setRotationAngle(0);
    }
  }, [speed]);

  const handleKeyPress = useCallback(
    (event) => {
      if (event.code === 'Space') {
        accelerate();
      }
    },
    [accelerate]
  );

  useEffect(() => {
    window.addEventListener('keydown', handleKeyPress);
    return () => {
      window.removeEventListener('keydown', handleKeyPress);
    };
  }, [handleKeyPress]);

  return (
    <div className="tachometer-container">
      <img
        src="https://cdn.pixabay.com/photo/2013/07/12/18/38/tachometer-153634_1280.png"
        alt="Tachometer Background"
        className="tachometer-bg"
      />
      <div
        className="arrow"
        style={{
          transform: `rotate(${rotationAngle}deg)`,
        }}
      ></div>
      <div className="speed-text">{speed} km/h</div>
      <div className="gear-text">Gear: {Math.floor(speed / 10) + 1}</div>
    </div>
  );
};

export default Tachometer;
