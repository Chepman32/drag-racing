// Car.js
import React from 'react';
import "./car.css";

const Car = ({ speed }) => {
  return (
    <div className='car'>
      <img className='body' src="https://i.stack.imgur.com/fSCKC.png" alt='' />
      <div className="wheelContainer">
        <img className='wheel frontWheel' src="https://i.pinimg.com/originals/f4/8f/ea/f48fea746a31d600627605dcfe902b58.png" style={{ transform: `rotate(${speed}deg)` }} alt='' />
        <img className='wheel rearWheel' src="https://i.pinimg.com/originals/f4/8f/ea/f48fea746a31d600627605dcfe902b58.png" style={{ transform: `rotate(${speed}deg)` }} alt='' />
      </div>
    </div>
  );
};

export default Car;
