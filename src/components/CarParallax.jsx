import React, { useState, useEffect } from 'react';
import { Parallax } from 'react-parallax';

const App = () => {
  const [scrollSpeed, setScrollSpeed] = useState(1);

  useEffect(() => {
    const interval = setInterval(() => {
      setScrollSpeed((prevSpeed) => prevSpeed + 1);
    }, 3000); // Increase speed every 3 seconds

    return () => clearInterval(interval);
  }, []);

  return (
    <div className="app-container">
          <Parallax
        bgImage="https://media.istockphoto.com/id/1081596948/vector/side-view-of-a-road-with-a-crash-barrier-roadside-green-meadow-and-clear-blue-sky-background.jpg?s=612x612&w=0&k=20&c=dikiJHfzZaDSy9Of4aMmEkqLxXMnHda2QqPguymZf-w="
        strength={scrollSpeed}
        style={{
          height: '100vh', // Set height to full viewport height
          width: '200%', // Double the width to create infinite effect
        }}
      >
        <div style={{ height: '100vh', width: '50%', float: 'left' }}>
          {/* Left Side Content (if any) */}
        </div>
        <div style={{ height: '100vh', width: '50%', float: 'left' }}>
          <img
            src="https://i.stack.imgur.com/fSCKC.png"
            alt="Car"
            style={{
              position: 'relative',
              top: '50%',
              transform: 'translateY(-50%)',
              width: '100px', // Adjust size as needed
              height: '50px',
            }}
          />
        </div>
      </Parallax>
    </div>
  );
};

export default App;
