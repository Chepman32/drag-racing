import React from 'react';
import { BrowserRouter as Router, Route, Routes, Link } from 'react-router-dom';
import MainMenu from './components/MainMenu';
import Garage from './components/Garage';
import RaceTrack from './components/RaceTrack';
import CarParallax from './components/CarParallax';

function App() {
  return (
    <Router>
      <div className="App">
      <RaceTrack/>
      </div>
    </Router>
  );
}

export default App;
